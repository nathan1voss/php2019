<?php

class Debug
{
    private $_debug = false;
    private $_msgList = [];

    public function __construct($debug = false){
        $this->setDebug($debug);
    }

    public function isDebug()
    {
        return $this->_debug;
    }

    public function setDebug($debug = true)
    {
        if(!$debug || is_null($debug) || empty($debug)) $this->_debug = false;
        else $this->_debug = true;
    }

    public function clear(){
        $this->_msgList = [];
    }

    public function addMsg($msg){
        array_push($this->_msgList, $msg);
    }

    public function mPr($tab){
        return '<pre>' . print_r($tab, true) . '</pre>';
    }

    public function send(){
        return $this->mPr($this->_msgList);
    }
}
