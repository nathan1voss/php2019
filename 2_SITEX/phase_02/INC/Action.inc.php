<?php

require_once 'Debug.inc.php';

class Action
{
    private $_list = [];
    private $_iDebug = null;

    public function __construct($iAct = null)
    {
        if($iAct instanceof Debug) $this->_iDebug = $iAct;
        else $this->_iDebug = new Debug();
    }

    public function add($actionName, $actionDatas)
    {
        array_push($this->_list, [$actionName => $actionDatas]);
        $this->_iDebug->addMsg('Ajout de l\'action : ' . $actionName);
    }

    public function send()
    {
        if($this->_iDebug->isDebug()){
            $this->_iDebug->addMsg('Ajout de l\'action : debug');
            $this->add('debug', $this->_iDebug->send());
        }
        return json_encode($this->_list, JSON_UNESCAPED_SLASHES);
    }
}
