<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="fr"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="fr"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<?php
    $metaNames = array(
        'author' => $infos['pNom'],
'description' => 'EPHEC-T2112 - site d\'examen ' . $infos['anacad'],
'version' => $infos['anacad'] . '-' . $infos['shortName'] . '-' . $infos['version'],
'viewport' => 'width=device-width, initial-scale=1'
);

$links = [
['rel' => 'icon', 'href' => 'favicon.ico', 'type' => 'image/gif'],
['rel' => 'apple-touch-icon', 'href' => 'IMG/apple-touch-icon.png'],
['rel' => 'stylesheet', 'href' => 'CSS/normalize.min.css'],
['rel' => 'stylesheet', 'href' => 'CSS/main.css'],
['rel' => 'stylesheet', 'href' => '/all/dTable/datatables.min.css'],
['rel' => 'stylesheet', 'href' => '/all/jQui/jquery-ui.min.css'],
['rel' => 'stylesheet', 'href' => 'CSS/custom.css'],
['rel' => 'stylesheet', 'href' => 'CSS/mediaQueries.css'],
];

$scripts = [
"/all/mo/modernizr.min.js",
"/all/jQ/jquery.min.js",
"/all/jQui/jquery-ui.min.js",
"JS/test.js",
"/all/dTable/datatables.min.js",
"JS/main.js"
]
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= $param['titleText'] ?></title>

    <?php foreach($metaNames as $name => $content): ?>
    <meta name="<?= $name ?>" content="<?= $content ?>">
    <?php endforeach; ?>

    <?php foreach($links as $linkAttr): ?>
    <link <?php foreach($linkAttr as $attr => $value): ?><?= $attr ?>="<?= $value ?>"<?php endforeach; ?>>
    <?php endforeach; ?>

    <?php foreach($scripts as $src): ?>
    <script src="<?= $src ?>"></script>
    <?php endforeach; ?>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade" lang="en">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="header-container">
    <header class="wrapper clearfix">
        <h1 class="title"><?= $param['siteName'] ?></h1>
        <nav id="menu">
            <?= $param['navContent'] ?>
        </nav>
    </header>
</div>

<div class="main-container">
    <div class="main wrapper clearfix">

        <article>
            <?= $param['mainContent'] ?>
        </article>

        <aside>
            <?= $param['asideContent'] ?>
        </aside>

    </div> <!-- #main -->
</div> <!-- #main-container -->

<div class="footer-container">
    <footer class="wrapper">
        <section id="infos">
            <?= $param['footerSection'] ?>
        </section>
    </footer>
</div>

<div id="error" hidden>
    <div id="phpError">

    </div>

    <div id="debug">

    </div>

    <div id="jsonError_">

    </div>
</div>
<?php
        if(error_get_last()){
            $rq->noDebug();
$testPhpError = $rq->send();
echo '<script>' . 'playActions(JSON.stringify(' . $testPhpError . '));</script>';
}
?>
</body>
</html>
