<?php
/**
 * User: Nathan
 * Date: 18-02-19
 * Time: 10:55
 * retourne le contenu du footer
 * @return string contenant le footer de notre page web
 */

function footerContent(){
    include 'dbConnect.inc.php';
    /** @var superglobal $__INFOS__ Contain all important infos */
    return "<a href=\"mailto:" . ___MATRICULE___ . "@students.ephec.be?Subject=PHP%202019\"target=\"_top\">" . $__INFOS__['prenom'] . "  " . $__INFOS__['nom'] . "</a> - <span id=\"groupeEphec\">2TL1</span>- @ 20".substr($__INFOS__['dbName'], -10, 2). "</section>";
}