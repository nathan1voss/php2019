<?php
/**
 * Created by PhpStorm.
 * User: Nathan
 * Date: 18-02-19
 * Time: 10:11
 */
include 'INC/custom.lib.php';
//Variables
$param = array(
    "siteName" => "Nom de site en param",
    "titleText" => "SITEX1819.00",
    "mainContent" => "<header>
                        <h1>Bienvenue</h1>
                        <p lang=\"En\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </header>
                    <section hidden>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </section>
                    <section hidden>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </section hidden>
                    <footer hidden>
                        <h3>article footer h3</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </footer>",
    "asideContent" => "<h3>aside</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "footerSection" => footerContent(),
    "navContent" => "<ul>
                        <li><a href=\"#\">accueil</a></li>
                        <li><a href=\"#\">tp</a></li>
                        <li><a href=\"#\">connexion</a></li>
                    </ul>",
);
//echo $param["siteName"];
//echo print_r($param, 1);
include 'INC/template.inc.html';