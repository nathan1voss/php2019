<?php

require_once 'Debug.inc.php';
require_once 'Action.inc.php';

class Request
{
    private $_rq = null;
    private $_iDebug = null;
    private $_rqList = [
        'test',
        'home'
    ];
    private $_iAction = null;

    public function __construct(){
        $this->_iDebug = new Debug(true);
        $this->_iAction = new Action($this->_iDebug);

        if(isset($_GET['rq'])) $this->_rq = $_GET['rq'];

        $this->_iDebug->addMsg('Requête reçue : ' . $this->_rq);

        if($this->isValid($this->_rq)){
            $this->_iDebug->addMsg('Requête valide !');

            $functionName = $this->_rq;
            $this->$functionName();
        }
        else $this->_iDebug->addMsg('Requête non valide !');
    }

    public function getRq(){
        return $this->_rq;
    }

    public function send(){
        if($phpError = error_get_last()) $this->_iAction->add('phpError', $phpError);

        return $this->_iAction->send();
    }

    public function isValid($rq){
        if(in_array($rq, $this->_rqList)) return true;
        return false;
    }

    public function noDebug(){
        $this->_iDebug->setDebug(false);
    }

    private function test(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
    }

    private function home(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        $oui = '<h1>Test de la chaine complête</h1><strong>Cela fonctionne !</strong>';
        $this->_iAction->add('testAffiche', $oui);
    }
}
