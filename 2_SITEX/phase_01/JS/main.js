// When DOM is loaded, do
$(function(){
    makeEvent();
    $('#error').on('dblclick', function(){
        $(this).hide(500);
    })
});

function makeRequest(event){
    event.preventDefault();
    $('#jsonError_, #debug, #phpError').empty();
    let request = $(this).attr('href').split('.')[0];
    console.log(request);
    let data2send = {
        'request': request
    };

    $.post('index.php?rq=' + request, data2send, playActions);
}

function playActions(retour){
    retour = parseJSON(retour);
    console.log(retour);

    retour.forEach(function(action){
        $.each(action, function(actionName, actionDatas){
            switch(actionName){
                case 'testAffiche':
                    let articleSelector = 'article';
                    $(articleSelector).html(actionDatas);

                    if(actionDatas.indexOf('<a ') !== -1) makeEvent(articleSelector);
                    break;

                case 'testLogOn':
                    let connectionSelector = '#menu ul li:eq(2)';
                    $(connectionSelector).html(actionDatas);

                    if(actionDatas.indexOf('<a ') !== -1) makeEvent(connectionSelector);
                    break;

                case 'testLogOff':
                    let disconnectionSelector = '#menu ul li:eq(2)';
                    $(disconnectionSelector).html(actionDatas);

                    if(actionDatas.indexOf('<a ') !== -1) makeEvent(disconnectionSelector);
                    break;

                case 'testSousMenu':
                    let asideSelector = 'aside';
                    $(asideSelector).html(actionDatas);

                    if(actionDatas.indexOf('<a ') !== -1) makeEvent(asideSelector);
                    break;

                case 'debug':
                case 'phpError':
                case 'jsonError_':
                    $('#' + actionName).html(function(){
                        if(!(actionName === 'debug')) {

                            let content = '<dl>';

                            $.each(actionDatas, function (key, value) {
                                content += '<dt>' + key + '</dt>';
                                content += '<dd>' + value + '</dd>';
                            });

                            actionDatas = content + '</dl>';
                        }

                        return '<fieldset><legend>' + actionName + '</legend>' + actionDatas + '</fieldset>';
                    });

                    $('#error').show();

                    break;

                default:
                    console.log('%c Action inconnue : ' + actionName, 'color: red');
            }
        })

    })
}

function parseJSON(json){
    let parsed;

    try{
        parsed = JSON.parse(json);
    } catch(e){
        parsed = [
            {
                'jsonError_': {
                    'error': e,
                    'data': json
                }
            }
        ]
    }

    return parsed;
}

function makeEvent(place='html'){
    $(place + ' a:not([href^="mailto:"])').on('click', makeRequest);
}
