<section id="login">
    <form id="formLogin" class="myForm" name="formLogin" method="post" action="formSubmit.html">
        <fieldset class="masterFieldset">
            <legend>Connexion</legend>
            <label for="pseudo">Pseudo</label>
            <input type="text" id="pseudo" name="login[pseudo]"><br>

            <label for="password">Mot de passe</label>
            <input type="password" id="password" name="login[password]">
            <a id="logMdPP" href="pasvorto.html">(perdu ou oublié ?)</a><br>

            <input type="submit" value="connexion">
            <div id="loginResult">

            </div>
        </fieldset>
    </form>
</section>
