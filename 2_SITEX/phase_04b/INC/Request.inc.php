<?php

require_once 'Debug.inc.php';
require_once 'Action.inc.php';
require_once 'Session.inc.php';
require_once 'Db.inc.php';
require_once 'Config.inc.php';

class Request
{
    private $_rq = null;
    private $_iDebug = null;
    private $_rqList = [
        'test',
        'home',
        //'affSess',
        'affSessLog',
        'affSessUser',
        'clrSessLog',
        'clrSessStart',
        'testDb',
        'tpSem08p02',
        'formSubmit',
        'formTP08',
        'config',
        'modifConfig',
        'sendLogo',
        'logOn',
        'formLogin',
        'logOff'
    ];
    private $_iAction = null;
    private $_iSession = null;
    private $_iDb = null;
    private $_iCfg = null;

    public function __construct(){
        $this->_iDebug = new Debug(true);
        $this->_iAction = new Action($this->_iDebug);
        $this->_iSession = new Session($this->_iDebug);
        $this->_iCfg = new Config($this->_iDebug);
        //$this->_iDb = new Db($this->_iDebug);

        if ( !isset($this->_iSession->getSession()['cfg']) ) {
            $_SESSION['cfg'] = $this->_iCfg->load();
        }

        if(isset($_GET['rq'])) $this->_rq = $_GET['rq'];

        $this->playRequest($this->_rq);
    }

    public function getRq(){
        return $this->_rq;
    }

    public function send(){
        if($phpError = error_get_last()) $this->_iAction->add('phpError', $phpError);

        return $this->_iAction->send();
    }

    public function isValid($rq){
        if(in_array($rq, $this->_rqList)) return true;
        return false;
    }

    public function noDebug(){
        $this->_iDebug->setDebug(false);
    }

    private function loadTemplate($name = 'yololo')
    {
        $fullName = 'INC/template.' . strtolower($name) . '.inc.php';

        if (!file_exists($fullName)) {
            $this->_iDebug->addMsg("template non trouvé - $name -");
            return null;
        }

        return implode("\n", file($fullName));
    }

    private function test(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = <<<HTML
            <h3>Testeur de requête</h3>
            <input type="text" id="testRq" placeholder="?rq=...">
            <a href=""></a>
HTML;
        $this->_iAction->affiche($content, 'aside');
    }

    private function home(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = '<h1>Test de la chaine complête</h1><strong>Cela fonctionne !</strong>';
        $this->_iAction->affiche($content);
    }

    private function affSess(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = $this->_iDebug->mPr($this->_iSession->getSession());
        $this->_iAction->affiche($content);
    }

    private function affSessLog(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = $this->_iDebug->mPr($this->_iSession->getLogs());
        $this->_iAction->affiche($content);
    }

    private function affSessUser(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = $this->_iDebug->mPr($this->_iSession->getUser());
        $this->_iAction->affiche($content);
    }

    private function clrSessLog(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $this->_iSession->clearLogs();
    }

    private function clrSessStart(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $this->_iSession->clearStart();
    }

    private function testDb(){
        $this->_iDb = new Db($this->_iDebug);
        //$content = $this->_iDb->testCall();
        //$content = $this->_iDb->testCall_1P('3T');
        //$content = $this->_iDb->call('mc_coursesGroupId', ['321']);
        //$content = $this->_iDb->call('userProfil', ['5']);
        $content = $this->_iDb->call('whoIs', ['ano', 'anonyme']);
        if(!$this->_iDb->getException()) $this->_iAction->affiche($content, null, 'table');
    }

    private function tpSem08p02()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        //$this->_iAction->affiche('Requête TPsem08 bien reçue');
        //$this->_iAction->affiche($this->loadTemplate('tpsem08'));
        //$this->_iAction->formTP08($this->loadTemplate('tpsem08'));
        $datas = [];
        $iDb = new Db($this->_iDebug);

        $datas['html'] = $this->loadTemplate('tpsem08');
        $datas['json'] = json_encode($iDb->call('mc_allGroupWithParent'));

        $this->_iAction->formTP08($datas);
    }

    private function formSubmit()
    {
        $sRequest = '';
        if( isset($_POST['senderForm']) ) $sRequest = $_POST['senderForm'];
        $this->playRequest($sRequest, 'sRequête');
    }

    private function formTP08()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $iDb = new Db($this->_iDebug);
        $datas = $iDb->call('mc_coursesGroupId', [ $_POST['select'] ]);

        if ( empty($datas) ) {
            $this->_iAction->affiche('Pas de cours associé à ce groupe', '#tp08result div');
        } else {
            $this->_iAction->dataTable($datas, '#tp08result div');
        }
    }

    private function config()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        $this->_iCfg->load();
        $this->_iAction->affiche($this->_iCfg->display());
    }

    private function modifConfig()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        //$this->_iDebug->addMsg($this->_iDebug->mPr($_REQUEST));
        $this->_iCfg->save();
        $_SESSION['cfg'] = $this->_iCfg->load();
        $this->sendLogo();
        $this->sendTitle();
        $this->_iAction->cfgResult('configuration sauvegardée');
    }

    private function sendLogo()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $logoDirectory = $this->_iSession->getSession()['cfg']['SITE']['images'];
        $logoName = $this->_iSession->getSession()['cfg']['LOGO']['logo'];
        $path = "$logoDirectory/$logoName";

        $this->_iAction->logoPath($path);

        if ( isset($this->_iSession->getSession()['user'][0]) ) $this->_iAction->userInfos($_SESSION['user'][0]);
    }

    private function sendTitle()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        $this->_iAction->siteTitle($this->_iSession->getSession()['cfg']['SITE']['titre']);
    }

    private function logOn()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        $this->_iAction->affiche($this->loadTemplate('login'));
    }

    private function formLogin()
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        $this->_iDb = new Db($this->_iDebug);
        $user = $this->_iDb->call('whoIs', [$_POST['login']['pseudo'], $_POST['login']['password']]);

        if ($this->_iDb->getException()) return $this->_iDebug->addMsg($this->_iDb->getException());
        if ( empty($user) ) return $this->_iAction->loginResult('Login-password incorrect');

        //$this->_iAction->affiche($user, 'aside', 'table');
        $_SESSION['user'] = $user;
        $profil = $this->_iDb->call('userProfil', [$user[0]['id']]);
        $_SESSION['user'][0]['profil'] = $profil;
        //$this->_iAction->affiche($this->_iDebug->mPr($_SESSION['user']), 'aside');
        $this->_iAction->userConnected($_SESSION['user'][0]);
    }

    private function logOff()
    {
        if ( !isset($this->_iSession->getSession()['user'][0]) ) return;

        unset($_SESSION['user'][0]);
        $this->_iAction->byebye();
    }

    private function playRequest($rq = '', $txt = 'Requête')
    {
        $this->_iDebug->addMsg("$txt reçue : " . $rq);

        if($this->isValid($rq)){
            $this->_iDebug->addMsg("$txt valide !");
            $this->_iSession->addLog($rq);

            $functionName = $rq;
            $this->$functionName();
        }
        else{
            $this->_iDebug->addMsg("$txt non valide !");
            $this->_iSession->addLog('!' . $rq);
        }
    }
}
