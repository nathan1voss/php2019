<?php

require_once 'Debug.inc.php';

class Action
{
    private $_list = [];
    private $_iDebug = null;

    public function __construct($iDebug = null)
    {
        if($iDebug instanceof Debug) $this->_iDebug = $iDebug;
        else $this->_iDebug = new Debug();
    }

    public function add($actionName, $actionDatas)
    {
        array_push($this->_list, [$actionName => $actionDatas]);
        $this->_iDebug->addMsg('Ajout de l\'action : ' . $actionName);
    }

    public function send()
    {
        if($this->_iDebug->isDebug()){
            $this->_iDebug->addMsg('Ajout de l\'action : debug');
            $this->add('debug', $this->_iDebug->send());
        }
        return json_encode($this->_list, JSON_UNESCAPED_SLASHES);
    }

    public function affiche($content, $dest = null, $type = null){
        if(!$dest) $dest = '.main article';
        if(!$type) $type = 'html';

        $data = [
            'content' => $content,
            'dest' => $dest,
            'type' => $type
        ];
        $this->add('affiche', $data);
    }

    public function formTP08($content)
    {
        $this->add('formTP08', $content);
    }

    public function dataTable($data, $dest = null, $options = null)
    {
        if ( is_null($dest) ) $dest = 'article';

        $initOptions = [
            'language' => ['url' => '/all/dTable/DataTables-1.10.18/i18n/French.json']
        ];

        if ( is_null($options) ) $options = $initOptions;
        elseif ($options == 'noFlourish') $options = array_merge_recursive($initOptions, ['info' => false]);
        else {
            if ( !isset($options['language']) ) $options = array_merge_recursive($initOptions, $options);
        }

        $toSend = [
            'data' => $data,
            'dest' => $dest,
            'options' => $options
        ];
        $this->add('dataTable', $toSend);
    }

    public function logoPath($path)
    {
        $this->add('logoPath', "../$path");
    }

    public function siteTitle($titre)
    {
        $this->add('siteTitle', $titre);
    }

    public function cfgResult($text)
    {
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);
        $this->add('cfgResult', $text);
    }

    public function loginResult($text)
    {
        $this->add('loginResult', $text);
    }

    public function userConnected($data)
    {
        $this->add('userConnected', $data);
    }

    public function userInfos($data)
    {
        $this->add('userInfos', $data);
    }

    public function byebye()
    {
        $this->add('byebye', '');
    }
}
