<?php

require_once 'Debug.inc.php';

class Session
{
    private $_iDebug = null;

    public function __construct($iDebug = null){
        if($iDebug instanceof Debug) $this->_iDebug = $iDebug;
        else $this->_iDebug = new Debug(true);

        session_start();

        if(!$this->isStart()){
            $_SESSION['start'] = $this->getTime();
            $_SESSION['user'] = [];
            $_SESSION['logs'] = [];
        }
    }

    private function getTime(){
        return date('YmdHis');
    }

    private function isStart(){
        if(isset($_SESSION['start'])) return true;
        return false;
    }

    public function addLog($texte){
        array_push($_SESSION['logs'], $texte . '@' . $this->getTime());
    }

    public function getStart(){
        return $_SESSION['start'];
    }

    public function getUser(){
        return $_SESSION['user'];
    }

    public function getLogs(){
        return $_SESSION['logs'];
    }

    public function getSession(){
        return $_SESSION;
    }

    public function clearLogs(){
        $_SESSION['logs'] = [];
    }

    public function clearStart(){
        unset($_SESSION['start']);
    }
}

