let myDatas = {};

// When DOM is loaded, do
$(function(){
    $.get('?rq=sendLogo', playActions);

    makeEvent();
    $('#error').on('dblclick', function(){
        $(this).hide(500);
    })
});

function makeRequest(event){
    event.preventDefault();
    $('#jsonError_, #debug, #phpError').empty();

    $.ajaxSetup({
        processData: false,
        contentType: false
    });

    let request = 'noRequest';
    let data2send = new FormData();

    switch (true) {
        case Boolean(this.href):
            request = $(this).attr('href').split('.')[0];
            break;

        case Boolean(this.action):
            request = $(this).attr('action').split('.')[0];
            data2send = new FormData(this);
            data2send.append('senderForm', this.id);
            break;
    }

    console.log('rq : ' + request);

    data2send.append('request', request);
    $.post('?rq=' + request, data2send, playActions);
}

function playActions(retour){
    retour = parseJSON(retour);
    console.log(retour);

    retour.forEach(function(action){
        $.each(action, function(actionName, actionDatas){
            switch(actionName){
                case 'affiche':

                    switch(actionDatas['type']){
                        case 'html':
                            $(actionDatas['dest']).html(actionDatas['content']);
                            break;

                        case 'table':
                            $(actionDatas['dest'])
                                .html(makeTable(actionDatas['content']))
                                .find('table');

                            break;

                        default:
                            console.log('affichage-type(' + actionDatas['type'] + ') inconnu');
                    }

                    makeEvent(actionDatas['dest']);
                    break;

                case 'formTP08':
                    myDatas['allGroups'] = JSON.parse(actionDatas['json']);

                    $('article').html(actionDatas['html']);
                    $('#select')
                        .html( makeOptions(myDatas['allGroups'], 'nom', 'id', 'nomParent') )
                        .on('change', function() {
                            $('#formTP08').submit();
                        });

                    $('#textSearch')
                        .on('keypress', function(event) {
                            if (event.which === 13) {
                                event.preventDefault();
                            }
                        })
                        .on('keyup', filtering);

                    $('#formSearch').find('input:radio').on('change', function () {
                        let value = $(this).val();
                        $('#textSearch').removeClass().addClass(value).trigger('keyup');
                    });

                    makeEvent('article');
                    break;

                case 'dataTable':
                    let dataTable = makeTable(actionDatas['data']);

                    $(actionDatas['dest'])
                        .html(dataTable)
                        .find('table')
                        .DataTable(actionDatas['options']);

                    break;

                case 'logoPath':
                    updateLogo(actionDatas);
                    break;

                case 'siteTitle':
                    $('h1.title').html(actionDatas);
                    break;

                case 'cfgResult':
                    $('#cfgResult')
                        .html(actionDatas)
                        .dialog({
                            title: 'Configuration Form',
                            width: 'auto',
                            show: {
                                effect: 'blind',
                                duration: 400
                            },
                            hide: {
                                effect: 'explode',
                                duration: 750
                            }
                        });
                    break;

                case 'loginResult':
                    $('#loginResult')
                        .html(actionDatas)
                        .dialog({
                            title: 'Authentification',
                            width: 'auto',
                            show: {
                                effect: 'blind',
                                duration: 400
                            },
                            hide: {
                                effect: 'explode',
                                duration: 750
                            }
                        });
                    break;

                case 'userConnected':
                    myDatas['user'] = actionDatas;

                    $('article').html('<h1>Bienvenue « ' + myDatas['user']['pseudo'] + ' »</h1>');
                    $('#menu')
                        .find("[href='logOn.php']")
                        .html('déconnexion')
                        .attr('href', 'logOff.html');
                    $('.header-container').css('border-bottom-color', 'orange');
                    break;

                case 'userInfos':
                    myDatas['user'] = actionDatas;
                    break;

                case 'byebye':
                    $('aside')
                        .html('Au revoir ' + myDatas.user['pseudo'])
                        .dialog({
                            title: 'Déconnexion',
                            modal: true,
                            close: function(event, ui) {
                                location.reload();
                            }
                        });
                    break;

                case 'testAffiche':
                    let articleSelector = 'article';
                    $(articleSelector).html(actionDatas);

                    makeEvent(articleSelector);
                    break;

                case 'testLogOn':
                    let connectionSelector = '#menu ul li:eq(2)';
                    $(connectionSelector).html(actionDatas);

                    makeEvent(connectionSelector);
                    break;

                case 'testLogOff':
                    let disconnectionSelector = '#menu ul li:eq(2)';
                    $(disconnectionSelector).html(actionDatas);

                    makeEvent(disconnectionSelector);
                    break;

                case 'testSousMenu':
                    let asideSelector = 'aside';
                    $(asideSelector).html(actionDatas);

                    makeEvent(asideSelector);
                    break;

                case 'debug':
                case 'phpError':
                case 'jsonError_':
                    $('#' + actionName).html(function(){
                        if (!(actionName === 'debug')) {

                            let content = '<dl>';

                            $.each(actionDatas, function (key, value) {
                                content += '<dt>' + key + '</dt>';
                                content += '<dd>' + value + '</dd>';
                            });

                            actionDatas = content + '</dl>';
                        }

                        return '<fieldset><legend>' + actionName + '</legend>' + actionDatas + '</fieldset>';
                    });

                    $('#error').show();

                    break;

                default:
                    console.log('%c Action inconnue : ' + actionName, 'color: red');
            }
        })

    })
}

function filtering() {
    let value = $(this).val();

    switch ( $(this).parent().find(':radio:checked').val() ) {
        case 'B':
            value = '^' + value;
            break;
        case 'E':
            value = value + '$';
            break;
    }

    if( value.indexOf('?') !== -1 || value.indexOf('.') !== -1 ) {
        value = '\\' + value;
    }

    let regex = new RegExp(value, 'i');

    let filteredData = myDatas.allGroups.filter( function(x) {
        return x.nom.match(regex)
    });

    $('#select').html( makeOptions(filteredData, 'nom', 'id', 'nomParent') );
}

function parseJSON(json){
    let parsed;

    try{
        parsed = JSON.parse(json);
    } catch(e){
        parsed = [
            {
                'jsonError_': {
                    'error': e,
                    'data': json
                }
            }
        ]
    }

    return parsed;
}

function makeEvent(place='html'){
    $(place + ' a:not([href^="mailto:"])').on('click', makeRequest);

    $(place + ' #testRq').on('change', function(){
        $(this).next()
            .attr('href', this.value + '.php')
            .click()
    });

    $(place + ' form').on('submit', makeRequest);
}

function makeTable(struct, index = false){

    if($.isEmptyObject(struct)){
        console.log('%c --- NO DATA --- in makeTable()', 'color: darkred');
        return '';
    }

    let out = '';

    let keys = Object.keys(struct);
    let nestedKeys = Object.keys(struct[keys[0]]);
    let sortedKeys;

    let structType = struct.constructor.name;
    if(structType === "Object") sortedKeys = keys.sort();
    if(structType === "Array") sortedKeys = keys.sort(function(a, b){ return a - b });

    out += '<table>';
    out += '<thead>';
    out += '<tr>';

    if(index) out += '<th>' + 'id' + '</th>';

    $.each(nestedKeys, function(i, value){
        out += '<th>' + value + '</th>';
    });

    out += '</tr>';
    out += '</thead>';
    out += '<tbody>';

    $.each(sortedKeys, function(i, value){
        out += '<tr>';

        if(index) out += '<td>' + value + '</td>';

        $.each(struct[value], function(i, value){
            out += '<td>' + value + '</td>';
        });
        out += '</tr>';
    });

    out += '</tbody>';
    out += '</table>';

    return out;
}

function makeSelect(struct, text, value = '', title = null){
    return '<select>' + makeOptions(struct, text, value, title) + '</select>';
}

function makeOptions(struct, text, value = '', title)
{
    if($.isEmptyObject(struct)){
        console.log('%c --- NO DATA --- in makeOptions()', 'color: darkred');
        return '';
    }

    if(!text){
        console.log('%c --- param text needed "' + text + '" --- in makeOptions()', 'color: darkred');
        return '';
    }

    if(!title){
        console.log('%c --- param title needed "' + title + '" --- in makeOptions()', 'color: darkred');
        return '';
    }

    let keys = Object.keys(struct);

    if( !(text in struct[keys[0]]) ){
        console.log('%c --- column text non exist "' + text + '" --- in makeOptions()', 'color: darkred');
        return '';
    }

    if( !(title in struct[keys[0]]) ){
        console.log('%c --- column title non exist "' + title + '" --- in makeOptions()', 'color: darkred');
        return '';
    }

    if (value !== '' && value !=='null' && !(value in struct[keys[0]])){
        console.log('%c --- column value non exist "' + value + '" --- in makeOptions()', 'color: darkred');
        return '';
    }

    let out = '';

    switch(value) {
        case 'null':
            $.each(keys, function () {
                if(struct[this][title] === null) struct[this][title] = '----';
                out += '<option value=\"' + this + '\" title=\"' + struct[this][title] + '\">' + struct[this][text] + '</option>';
            });
            break;

        case '':
            value = text;

        default:
            $.each(keys, function () {
                if(struct[this][title] === null) struct[this][title] = '----';
                out += '<option value=\"' + struct[this][value] + '\" title=\"' + struct[this][title] + '\">' + struct[this][text] + '</option>';
            });
            break;
    }

    return out;
}

function updateLogo(pathfile)
{
    let stylesheets     = document.styleSheets;
    let stylesheetsKeys = Object.keys(stylesheets);

    let fileKey = stylesheetsKeys.find(function(element) {
        if (stylesheets[element]['href'].indexOf('custom.css') !== -1) return element;
    });

    let rules     = stylesheets[fileKey]['cssRules'];
    let rulesKeys = Object.keys(rules);

    let selectorKey = rulesKeys.find(function(element) {
        if (rules[element]['selectorText'].indexOf('.title::before') !== -1) return element;
    });

    stylesheets[fileKey].cssRules[selectorKey].style.backgroundImage = 'url(' + pathfile + ')';
}
