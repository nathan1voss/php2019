function testLoad(request){
    $("article").load('../../../../RES/appelAjax.php?rq=' + request);
}

function testGet(request){
    $.get('../../../../RES/appelAjax.php', { rq: request}, function(data){
        $("article").html(data);
    })
}

function testPost(request){
    $.post('../../../../RES/appelAjax.php', { rq: request}, function(data){
        $("article").html(data);
    })
}

function testGetJSON(request){
    $.getJSON('../../../../RES/appelAjax.php', { rq: request}, function(data){
        let defList = '<dl>';

        $.each(data, function(livreId, livreInfos){
            defList += '<dt>id : ' + livreId + '</dt>';
            $.each(livreInfos, function(key, value){
                defList += '<dd>' + key + ' : ' + value + '</dd>';
            });
        });

        defList += '</dl>';

        $("article").html(defList);
    })
}

(function($){
    $.postJSON = function(url, data, callback) {
        return $.post(url, data, callback, 'json');
    }
})(jQuery);

function testPostJSON(request){
    $.postJSON('../../../../RES/appelAjax.php', { rq: request}, function(data){
        let defList = '<dl>';

        $.each(data, function(livreId, livreInfos){
            defList += '<dt>id : ' + livreId + '</dt>';
            $.each(livreInfos, function(key, value){
                defList += '<dd>' + key + ' : ' + value + '</dd>';
            });
        });

        defList += '</dl>';

        $("article").html(defList);
    })
}

(function($){
    $.postAjaxJSON = function (url, data, callback) {
        return $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: data,
            success: callback
        });
    }
})(jQuery);

function testAjaxJSON(request){
    $.postAjaxJSON('../../../../RES/appelAjax.php', { rq: request}, function(data){
        let defList = '<dl>';

        $.each(data, function(livreId, livreInfos){
            defList += '<dt>id : ' + livreId + '</dt>';
            $.each(livreInfos, function(key, value){
                defList += '<dd>' + key + ' : ' + value + '</dd>';
            });
        });

        defList += '</dl>';

        $("article").html(defList);
    })
}

function testParseJson(json){
    let parsed;

    try{
        parsed = JSON.parse(json);
    } catch(e){
        parsed = {
            'jsonError_': {
                'error': e,
                'data': json
            }
        }
    }

    return parsed;
}

function callbackTestJson(data){
    let retour = testParseJson(data);
    console.log(data);

    let defList = '<dl>';
    if(Object.keys(retour)[0] == "jsonError_"){
        $.each(retour, function(key, value){
            defList += '<dt>' + key + '</dt>';
            $.each(value, function(key, value){
                defList += '<dd>' + key + ' : ' + value + '</dd>';
            })
        })
    } else{
        $.each(retour, function(livreId, livreInfos){
            defList += '<dt>id : ' + livreId + '</dt>';
            $.each(livreInfos, function(key, value){
                defList += '<dd>' + key + ' : ' + value + '</dd>';
            });
        });
    }
    defList += '</dl>';

    $("article").html(defList);
}

function testPostTestJSON(request){
    $.post('../../../../RES/appelAjax.php', { rq: request}, callbackTestJson)
}

function makeTable_v1(struct){

    if($.isEmptyObject(struct)){
        console.log('%c --- NO DATA --- in makeTable_v1()', 'color: darkred')
        return '';
    }

    let out = '';

    let keys = Object.keys(struct);
    let nestedKeys = Object.keys(struct[keys[0]]);
    let sortedKeys;

    let structType = struct.constructor.name;
    if(structType === "Object") sortedKeys = keys.sort();
    if(structType === "Array") sortedKeys = keys.sort(function(a, b){ return a - b });

    out += '<table>';
    out += '<thead>';
    out += '<tr>';
    out += '<th>' + 'id' + '</th>';

    $.each(nestedKeys, function(){
        out += '<th>' + this + '</th>';
    });

    out += '</tr>';
    out += '</thead>';
    out += '<tbody>';

    $.each(sortedKeys, function(){
        out += '<tr>';
        out += '<td>' + this + '</td>';

        $.each(struct[this], function(){
            out += '<td>' + this + '</td>';
        });
        out += '</tr>';
    });

    out += '</tbody>';
    out += '</table>';

    return out;

}

function testGetJSON_v2(request){
    $.getJSON('../../../../RES/appelAjax.php', { rq: request}, function(data){
        $("article").html(makeTable_v2(data, true));
    });
}

function makeTable_v2(struct, index = false){

    if($.isEmptyObject(struct)){
        console.log('%c --- NO DATA --- in makeTable_v2()', 'color: darkred');
        return '';
    }

    let out = '';

    let keys = Object.keys(struct);
    let nestedKeys = Object.keys(struct[keys[0]]);
    let sortedKeys;

    let structType = struct.constructor.name;
    if(structType === "Object") sortedKeys = keys.sort();
    if(structType === "Array") sortedKeys = keys.sort(function(a, b){ return a - b });

    out += '<table>';
    out += '<thead>';
    out += '<tr>';

    if(index) out += '<th>' + 'id' + '</th>';

    $.each(nestedKeys, function(){
        out += '<th>' + this + '</th>';
    });

    out += '</tr>';
    out += '</thead>';
    out += '<tbody>';

    $.each(sortedKeys, function(){
        out += '<tr>';

        if(index) out += '<td>' + this + '</td>';

        $.each(struct[this], function(){
            out += '<td>' + this + '</td>';
        });
        out += '</tr>';
    });

    out += '</tbody>';
    out += '</table>';

    return out;

}

function makeSelect_v1(struct, text, value = ''){

    if($.isEmptyObject(struct)){
        console.log('%c --- NO DATA --- in makeSelect_v1()', 'color: darkred')
        return '';
    }

    if(!text){
        console.log('%c --- param text needed "' + text + '" --- in makeSelect_v1()', 'color: darkred')
        return '';
    }

    let keys = Object.keys(struct);

    if( !(text in struct[keys[0]]) ){
        console.log('%c --- column text non exist "' + text + '" --- in makeSelect_v1()', 'color: darkred')
        return '';
    }

    if (value !== '' && value !=='null' && !(value in struct[keys[0]]) ){
        console.log('%c --- column value non exist "' + value + '" --- in makeSelect_v1()', 'color: darkred')
        return '';
    }

    let out = '<select>';

    switch(value){
        case 'null':
            $.each(keys, function(){
                out += '<option value=\"' + this + '\">' + struct[this][text] + '</option>';
            });
            break;

        case '':
            value = text;

        default:
            $.each(keys, function(){
                out += '<option value=\"' + struct[this][value] + '\">' + struct[this][text] + '</option>';
            });
            break;
    }

    out += '</select>';

    return out;

}

function testGetJSON_v3(request, text, value){
    $.getJSON('../../../../RES/appelAjax.php', { rq: request}, function(data){
        $("article").html(makeSelect_v1(data, text, value));
    });
}

