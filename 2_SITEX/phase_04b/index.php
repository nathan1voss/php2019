<?php

require_once 'INC/custom.lib.php';

$infos = getInfos('SITEX', '04a');

require_once 'INC/Request.inc.php';

$rq = new Request();
if(!is_null($rq->getRq())) die($rq->send());

$param = array(
    'siteName' => $_SESSION['cfg']['SITE']['titre'],
    'titleText' => $infos['shortName'] . $infos['anacad'] . '.' . $infos['version'],
    'mainContent' => '<header>
                        <h1>Bienvenue</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </header>
                      <section hidden>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </section>
                      <section hidden>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </section>
                      <footer hidden>
                        <h3>article footer h3</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </footer>',
    'asideContent' => '<h3>aside</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
    'footerSection' => footerContent($infos),
    'userStyle' => '',
    'connectLink' => '<a href="logOn.php">connexion</a>'
);

if ( isset($_SESSION['user'][0]) ) {
    $param['mainContent'] = "<h2>Page rafraichie : vous êtes toujours connecté {$_SESSION['user'][0]['pseudo']} !</h2>";
    $param['userStyle'] = 'border-bottom-color: orange;';
    $param['connectLink'] = '<a href="logOff.php">déconnexion</a>';
}

$param['navContent'] = '<ul>
                        <li><a href="home.php">accueil</a></li>
                        <li><a href="test.html">tp</a></li>
                        <li><a href="tpSem08p02.html">s08p2</a></li>
                        <li>' . $param['connectLink'] . '</li>
                    </ul>';

require_once 'INC/template.inc.php';
