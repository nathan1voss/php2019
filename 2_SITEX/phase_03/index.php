<?php

require_once 'INC/custom.lib.php';

$infos = getInfos('SITEX', '02');

require_once 'INC/Request.inc.php';


$rq = new Request();
if(!is_null($rq->getRq())) die($rq->send());


//Variables
$param = array(
    'siteName' => 'Nom de site en param',
    'titleText' => $infos['shortName'] . $infos['anacad'] . '.' . $infos['version'],
    'mainContent' => '<header>
                        <h1>Bienvenue</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </header>
                      <section hidden>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </section>
                      <section hidden>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </section>
                      <footer hidden>
                        <h3>article footer h3</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </footer>',

    'asideContent' => '<h3>aside</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
    'footerSection' => footerContent($infos),
    'navContent' => '<ul>
                        <li><a href="home.php">accueil</a></li>
                        <li><a href="test.html">tp</a></li>
                        <li><a href="logOn.php">connexion</a></li>
                    </ul>'
);
//echo $param["siteName"];
//echo print_r($param, 1);
require_once 'INC/template.inc.html';
//$infos = getInfos('SITEX1819.00',$metaNames['version'],'INC/dbConnect.inc.php');
