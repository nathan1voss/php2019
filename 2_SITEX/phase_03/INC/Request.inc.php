<?php

require_once 'Debug.inc.php';
require_once 'Action.inc.php';
require_once 'Session.inc.php';
require_once 'Db.inc.php';

class Request
{
    private $_rq = null;
    private $_iDebug = null;
    private $_rqList = [
        'test',
        'home',
        'affSess',
        'affSessLog',
        'affSessUser',
        'clrSessLog',
        'clrSessStart',
        'testDb'
    ];
    private $_iAction = null;
    private $_iSession = null;
    private $_iDb = null;

    public function __construct(){
        $this->_iDebug = new Debug(true);
        $this->_iAction = new Action($this->_iDebug);
        $this->_iSession = new Session($this->_iDebug);
        $this->_iDb = new Db($this->_iDebug);

        if(isset($_GET['rq'])) $this->_rq = $_GET['rq'];

        $this->_iDebug->addMsg('Requête reçue : ' . $this->_rq);

        if($this->isValid($this->_rq)){
            $this->_iDebug->addMsg('Requête valide !');
            $this->_iSession->addLog($this->_rq);

            $functionName = $this->_rq;
            $this->$functionName();
        }
        else{
            $this->_iDebug->addMsg('Requête non valide !');
            $this->_iSession->addLog('!' . $this->_rq);
        }
    }

    public function getRq(){
        return $this->_rq;
    }

    public function send(){
        if($phpError = error_get_last()) $this->_iAction->add('phpError', $phpError);

        return $this->_iAction->send();
    }

    public function isValid($rq){
        if(in_array($rq, $this->_rqList)) return true;
        return false;
    }

    public function noDebug(){
        $this->_iDebug->setDebug(false);
    }

    private function test(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = <<<HTML
            <h3>Testeur de requête</h3>
            <input type="text" id="testRq" placeholder="?rq=...">
            <a href=""></a>
HTML;
        $this->_iAction->affiche($content, 'aside');
    }

    private function home(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = '<h1>Test de la chaine complête</h1><strong>Cela fonctionne !</strong>';
        $this->_iAction->affiche($content);
    }

    private function affSess(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = $this->_iDebug->mPr($this->_iSession->getSession());
        $this->_iAction->affiche($content);
    }

    private function affSessLog(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = $this->_iDebug->mPr($this->_iSession->getLogs());
        $this->_iAction->affiche($content);
    }

    private function affSessUser(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $content = $this->_iDebug->mPr($this->_iSession->getUser());
        $this->_iAction->affiche($content);
    }

    private function clrSessLog(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $this->_iSession->clearLogs();
    }

    private function clrSessStart(){
        $this->_iDebug->addMsg('Je suis dans ' . __FUNCTION__);

        $this->_iSession->clearStart();
    }

    private function testDb(){
        $this->_iDb = new Db($this->_iDebug);
        //$content = $this->_iDb->testCall();
        //$content = $this->_iDb->testCall_1P('3T');
        $content = $this->_iDb->call('mc_coursesGroupId', ['321']);
        if(!$this->_iDb->getException()) $this->_iAction->affiche($content, null, 'table');
    }
}

