[ERREUR]
interdit = "<?php die('Vous n\'êtes pas autorisé à voir ce contenu&quot;'); ?>"

[SITE]
titre  = "Site Perso"
images = img

[LOGO]
logo   = "logo.PNG"
taille = 100
min    = 50
max    = 125
pas    = 5

[DB]
host   = localhost
user   = VOSS
pswd   = NathanB2r4
dbname = 1819he201286

[AVATAR]
comment = "le dossier devra se trouver dans celui des images repris dans SITE"
dossier = avatar
anonyme = "unknown.png"
min     = 100
taille  = 150
max     = 200
type    = "jpg|gif|png"
choix[] = jpg
choix[] = png
